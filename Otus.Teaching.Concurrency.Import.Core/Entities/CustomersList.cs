﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    [Serializable, XmlRoot("Customers")]
    public class CustomersList
    {
        public List<Customer> Customers { get; set; }
    }
}