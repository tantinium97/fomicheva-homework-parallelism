using System;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    [Serializable]
    public sealed class Customer
    {
        [XmlElement("Id")]
        public int Id { get; set; }

        [XmlElement("FullName")]
        public string FullName { get; set; }

        [XmlElement("Email")]
        public string Email { get; set; }

        [XmlElement("Phone")]
        public string Phone { get; set; }
    }
}