﻿using System.Threading.Tasks;
namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDataLoader
    {
        public Task LoadData();
    }
}