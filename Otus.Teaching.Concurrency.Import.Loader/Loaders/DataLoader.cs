﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System.Diagnostics;
using System;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoader : IDataLoader
    {
        private readonly int _threadCount;
        private readonly CustomersList _customers;

        public DataLoader(CustomersList customers, int threadCount)
        {
            _customers = customers;
            _threadCount = threadCount;
        }

        public async Task LoadData()
        {
            Stopwatch watch = new();
            watch.Start();

            int size = _customers.Customers.Count /_threadCount;
            int remainder = _customers.Customers.Count % _threadCount;
            int start = 0;
            int param = 1;

            Task[] tasks;

            if(_threadCount <= _customers.Customers.Count)
            {
                tasks = new Task[_threadCount];
                for (var i = 0; i < _threadCount; i++)
                {
                    if (remainder == 0) param = 0;
                    var customer = DivList(_customers.Customers, start, start + size + param);
                    start += size + param;
                    remainder--;
                    tasks[i] = AddCustomersAsync(customer);
                }
            }
            else
            {
                tasks = new Task[_customers.Customers.Count];
                for (var i = 0; i < _customers.Customers.Count; i++)
                {
                    var customer = DivList(_customers.Customers, start, start + 1);
                    start++;
                    tasks[i] = AddCustomersAsync(customer);
                }
            }
            await Task.WhenAll(tasks);
            watch.Stop();
            Console.WriteLine($"Время загрузки: {GetTimeFormat(watch.Elapsed)}");
        }

        public static CustomersList DivList(List<Customer> list, int iStart, int iStop)
        {
            var customersList = new CustomersList() { Customers = new List<Customer>()};

            if (iStop <= list.Count)
            {
                while (iStart < iStop)
                {
                    customersList.Customers.Add(list[iStart]);
                    iStart++;
                }
            }
            else
            {
                while (iStart < list.Count)
                {
                    customersList.Customers.Add(list[iStart]);
                    iStart++;
                }
            }
            return customersList;
        }

        private static async Task AddCustomersAsync(CustomersList list)
        {
            await new CustomerRepository().AddCustomersAsync(list);
        }

        private static string GetTimeFormat(TimeSpan time)
        {
            return string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            time.Hours, time.Minutes, time.Seconds, time.Milliseconds);
        }
    }
}
