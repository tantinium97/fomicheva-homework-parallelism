using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader : IDataLoader
    {
        public async Task LoadData()
        {
            Console.WriteLine("Loading data...");
            await Task.Delay(10000);
            Console.WriteLine("Loaded data...");
        }
    }
}