﻿using System;
using System.Diagnostics;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    static class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName = Path.Combine(_dataFileDirectory, "customers.xml");
        private static bool _isGenerateByMethod = true;
        private static string _name;
        private static string _generatorExe;
        private static int _dataCount;
        private static int _threads;
        private static Process myProcess;
        private static TaskCompletionSource<bool> eventHandled;

        static async Task Main(string[] args)
        {
            var confBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            var config = confBuilder.Build();

            _generatorExe = config["generatorExe"];
            _dataCount = Convert.ToInt32(config["dataCount"]);
            _threads = Convert.ToInt32(config["threads"]);

            if (args?.Length == 1)
            {
                _isGenerateByMethod = false;
                _dataFileName = Path.Combine(_dataFileDirectory, $"{args[0]}.xml");
                _name = args[0];
            }

            Console.WriteLine($"Запущен загрузчик с идентификатором процесса {Environment.ProcessId}...");

            await GenerateCustomersDataFile();

            var customersList = new XmlParser(_dataFileName).Parse();
            var loader = new DataLoader(customersList, _threads);
            await loader.LoadData();
        }

        private static async Task GenerateCustomersDataFile()
        {
            if (_isGenerateByMethod)
            {
                var xmlGenerator = new XmlGenerator(_dataFileName, _dataCount);
                xmlGenerator.Generate();
                Console.WriteLine("Сгенерированы xml данные с помощью метода...");
            }
            else
            {
                eventHandled = new TaskCompletionSource<bool>();
                using (myProcess = new Process())
                {
                    try
                    {
                        myProcess.StartInfo.FileName = _generatorExe;
                        myProcess.StartInfo.ArgumentList.Add(Path.Combine(_dataFileDirectory,_name));
                        myProcess.StartInfo.ArgumentList.Add(_dataCount.ToString());
                        myProcess.EnableRaisingEvents = true;
                        myProcess.Exited += MyProcessExited;
                        myProcess.Start();
                        Console.WriteLine($"Сгенерированы xml данные с помощью процесса {myProcess.Id}...");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Произошла ошибка при генерации \"{_dataFileName}\":\n{ex.Message}");
                        return;
                    }

                    await Task.WhenAny(eventHandled.Task);
                }
            }
        }

        private static void MyProcessExited(object sender, EventArgs e)
        {
            Console.WriteLine($"Время генерации: {GetTimeFormat(myProcess.ExitTime - myProcess.StartTime)}");
            eventHandled.TrySetResult(true);
        }

        private static string GetTimeFormat(TimeSpan time)
        {
            return string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            time.Hours, time.Minutes, time.Seconds, time.Milliseconds);
        }
    }
}