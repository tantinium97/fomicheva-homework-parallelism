using System;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly ConsumerContext _context;
        private readonly int retryCount = 2;

        public CustomerRepository()
        {
            _context = new ConsumerContext();
        }

        public async Task AddCustomersAsync(CustomersList customers)
        {
            int currentRetry = 0;
            foreach (var item in customers.Customers)
            {
                await _context.Customers.AddAsync(item);
            }
            for (;;)
            {
                try
                {
                    await _context.SaveChangesAsync();
                    Console.WriteLine($"В базу данных добавлено клиентов ({customers.Customers.Count})");
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Ошибка сохранения в базу данных:");
                    Console.WriteLine($"EM: -> {ex.Message}");
                    await Task.Delay(2000);
                    currentRetry++;
                    Console.WriteLine($"\nПовторная попытка сохранения № {currentRetry}:");
                    if (currentRetry > retryCount)
                    {
                        Console.WriteLine("Лимит попыток исчерпан");
                        Console.WriteLine($"ES: -> {ex.StackTrace}");
                        throw;
                    }
                }
            }
        }
    }
}