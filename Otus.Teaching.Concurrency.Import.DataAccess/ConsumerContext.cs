using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess;

public sealed class ConsumerContext : DbContext
{
    #region Public Constructors

    public ConsumerContext()
    {
        Database.EnsureCreated();
    }

    #endregion

    #region Public Properties

    public DbSet<Customer> Customers { get; set; } = null!;

    #endregion

    #region Protected Methods

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(GetConnString());
    }

    #endregion

    #region Private Methods

    private static string GetConnString()
    {
        return "Host=localhost;Port=5432;Database=consumerdb;Username=postgres;Password=postgres";
    }

    #endregion
}