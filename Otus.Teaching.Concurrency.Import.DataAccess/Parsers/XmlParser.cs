﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<CustomersList>
    {
        private readonly string _dataFileName;
        public XmlParser(string dataFileName)
        {
            _dataFileName = dataFileName;
        }

        public CustomersList Parse()
        {
            using var reader = new StreamReader(_dataFileName);
            XmlSerializer serializer = new(typeof(CustomersList));
            return (CustomersList)serializer.Deserialize(reader);
        }
    }
}